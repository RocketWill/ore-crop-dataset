## ORE crop dataset

### 1. crop images and json files
```bash
 python crop_imgs_and_jsons.py [src_imgs_jsons_dir] [output_dir]
```

### 2. combine cropped images
- filename format: `test_id_cropID.jpg`, example: `test_22_1.jpg`

```bash
python combine_imgs.py [src_dir] [output_dir]
```