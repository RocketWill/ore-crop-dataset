#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""

import cv2
import ntpath
import os
import json
import base64
import glob
import argparse

def create_piece(section_width, section_height):
    return {
        "version": "4.2.10",
        "flags": {},
        "shapes": [],
        "imagePath": "",
        "imageHeight": section_height,
        "imageWidth": section_width
    }

def cteate_shape(points):
    return {
        "label": "ore",
        "points": points,
        "group_id": None,
        "shape_type": "polygon",
        "flags": {}
    }

def get_min_max(polygon):
    max_x = 0
    min_x = float('inf')
    max_y = 0
    min_y = float('inf')
    for point in polygon:
        x, y = point
        if x > max_x:
            max_x = x
        if x <= min_x:
            min_x = x
        if y > max_y:
            max_y = y
        if y <= min_y:
            min_y = y
    return [max_x, min_x, max_y, min_y]

def get_sections(width, height, horizontal_counts, vertical_counts):
    vertical_section_width = width // horizontal_counts
    horizontal_section_width = height // vertical_counts
    sections = []
    for j in range(horizontal_counts):
        for i in range(vertical_counts):
            w_l = i * horizontal_section_width
            w_r = (i+1) * horizontal_section_width
            h_l = j * vertical_section_width
            h_r = (j+1) * vertical_section_width
            sections.append((w_l, w_r, h_l, h_r))
    return sections

def cut_images(sections, image_path, output_dir):
    os.makedirs(output_dir, exist_ok=True)
    basename = ntpath.basename(image_path)
    file_name = os.path.splitext(basename)[0]
    extension = os.path.splitext(basename)[1]
    img = cv2.imread(image_path)
    for i in range(len(sections)):
        w_start, w_end, h_start, h_end = sections[i]
        crop_img = img[h_start:h_end, w_start:w_end]
        output_path = os.path.join(output_dir, file_name + '_' + str(i+1) + extension)
        cv2.imwrite(output_path, crop_img)
    print("crop image successfully")

def cut_points(points, w_start, w_end, h_start, h_end):
    result = []
    max_x, min_x, max_y, min_y = get_min_max(points)
    if min_x > w_start and max_x < w_end and min_y > h_start and max_y < h_end:
        for point in points:
            x, y = point
            result.append([x - w_start, y - h_start])
    return result

def cut_shape(shape_obj, w_start, w_end, h_start, h_end):
    points = shape_obj['points']
    result = cut_points(points, w_start, w_end, h_start, h_end)
    return result

if __name__ == '__main__':
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('src_folder', help='folder contains images and labelme jsons to be cropped')
    args_parser.add_argument('output_folder', help='cropped images output folder')
    args = args_parser.parse_args()

    # img_folder = '/Users/willc/Desktop/wattman/Ball/crop_data/ori'
    # output = '/Users/willc/Desktop/wattman/Ball/crop_data/cut'

    images = glob.glob(args.src_folder + '/*.jpg')
    os.makedirs(args.output_folder, exist_ok=True)
    sections = get_sections(1234, 1624, 3, 3)

    # crop images
    for img in images:
        cut_images(sections, img, args.output_folder)

    # crop json files
    all_jsons = glob.glob(args.src_folder + '/*.json')
    for jf in all_jsons:
        basename = ntpath.basename(jf)
        file_name = os.path.splitext(basename)[0]
        extension = os.path.splitext(basename)[1]

        with open(jf, 'r') as reader:
            jf = json.loads(reader.read())
            shapes = jf['shapes']

            for i in range(len(sections)):
                w_start, w_end, h_start, h_end = sections[i]
                piece = create_piece(541, 411)
                for shape in shapes:
                    s = cut_shape(shape, w_start, w_end, h_start, h_end)
                    if len(s) > 0:
                        piece['shapes'].append(cteate_shape(s))
                        piece['imagePath'] = file_name + "_" + str(i + 1) + '.jpg'
                        with open(args.output_folder + "/" + file_name + '_' + str(i + 1) + '.jpg', "rb") as imageFile:
                            img_data = base64.b64encode(imageFile.read())
                            piece['imageData'] = img_data.decode("utf-8")
                with open(args.output_folder + "/" + file_name + '_' + str(i + 1) + '.json', 'w') as outfile:
                    json.dump(piece, outfile)




