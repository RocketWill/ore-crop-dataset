#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""
"""
NOTICE: image named format for test: "test_id_cropID.jpg"
example: test_22_1.jpg
"""
import numpy as np
import glob
import ntpath
import cv2
import os
import argparse

def get_sections(width, height, horizontal_counts, vertical_counts):
    vertical_section_width = width // horizontal_counts
    horizontal_section_width = height // vertical_counts
    sections = []
    for j in range(horizontal_counts):
        for i in range(vertical_counts):
            w_l = i * horizontal_section_width
            w_r = (i+1) * horizontal_section_width
            h_l = j * vertical_section_width
            h_r = (j+1) * vertical_section_width
            sections.append((w_l, w_r, h_l, h_r))
    return sections

if __name__ == "__main__":
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('test_folder', help='test images folder')
    args_parser.add_argument('output_folder', help='test images output folder')
    args = args_parser.parse_args()

    img_shape = (1234, 1624, 3)
    sections = get_sections(1234, 1624, 3, 3)
    os.makedirs(args.output_folder, exist_ok=True)

    all_images = glob.glob(args.test_folder + '/*.jpg')
    all_img_id = list(set([ntpath.basename(s).split("_")[1] for s in all_images]))
    all_img_id = sorted(all_img_id, key=lambda x: (x))

    for id in all_img_id:
        m = np.zeros(img_shape, dtype=np.uint8)
        section_imgs = glob.glob(args.test_folder + '/*' + id + '*.jpg')
        section_imgs = sorted(section_imgs, key=lambda d: ntpath.basename(d))
        for i in range(len(section_imgs)):
            cv_img = cv2.imread(section_imgs[i])
            w_start, w_end, h_start, h_end = sections[i]
            m[h_start:h_end, w_start:w_end] = cv_img
            cv2.imwrite(os.path.join(args.output_folder, 'test_' + str(id) + '.jpg'), m)
    print("combine images successfully")